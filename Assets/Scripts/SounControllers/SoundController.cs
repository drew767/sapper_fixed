﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public Sprite m_spriteSoundOn;
    public Sprite m_spriteSoundOff;

    public Sprite m_spriteMusicOn;
    public Sprite m_spriteMusicOff;

    private bool m_soundIsOn;
    private bool m_musicIsOn;

    private float m_savedVolumeBeforeMute;

    public Image m_soundButtonImage;
    public Image m_musicButtonImage;

    public AudioMixer m_audioMixer;

    void Start()
    {
        //////////////REPLACE WITH LOADING FROM SAVES
        m_soundIsOn = true;
        m_musicIsOn = true;
        m_audioMixer.GetFloat("SoundsVolume", out m_savedVolumeBeforeMute);
        /////////////////////////////////////////////////////

        SetupBackgroundMusic();
        SetupSounds();
    }


    public void ToggleSound()
    {
        m_soundIsOn = !m_soundIsOn;
        SetupSounds();
    }
    public void ToggleBackgroundMusic()
    {
        m_musicIsOn = !m_musicIsOn;
        SetupBackgroundMusic();
    }


    public void SetSoundVolume(float volume)
    {
        m_audioMixer.SetFloat("SoundsVolume", volume);
    }
    private void SetupBackgroundMusic()
    {
        float volume = -80.0f;
        if (m_musicIsOn)
        {
            volume = 0;
        }
        m_audioMixer.SetFloat("BackgroundVolume", volume);
        SetCorrectSpriteForMusicButton();
    }

    private void SetCorrectSpriteForMusicButton()
    {
        if (m_musicIsOn && m_soundIsOn)
        {
            m_musicButtonImage.sprite = m_spriteMusicOn;
        }
        else
        {
            m_musicButtonImage.sprite = m_spriteMusicOff;
        }
    }

    private void SetCorrectSpriteForSoundButton()
    {
        if (m_soundIsOn)
        {
            m_soundButtonImage.sprite = m_spriteSoundOn;
        }
        else
        {
            m_soundButtonImage.sprite = m_spriteSoundOff;
        }
    }
    private void SetupSounds()
    {
        if (m_soundIsOn)
        {
            m_audioMixer.SetFloat("SoundsVolume", m_savedVolumeBeforeMute);
        }
        else
        {
            m_audioMixer.GetFloat("SoundsVolume", out m_savedVolumeBeforeMute);
            m_audioMixer.SetFloat("SoundsVolume", -80);
        }
        SetCorrectSpriteForSoundButton();
        SetCorrectSpriteForMusicButton();
    }
}
