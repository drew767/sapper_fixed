﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayoutManager : MonoBehaviour
{
    public GameObject m_pauseMenu;
    public GameObject m_game;
    public GameObject m_mainMenu;
    public GameObject m_levelsMenu;
    public GameObject m_lostMenu;
    public GameObject m_winMenu;

    void Start()
    {
        EventSystem.RegisterListener<LevelSelectionMenu>(OnLevelSelectionMenuCalled);
        EventSystem.RegisterListener<GameLost>(OnGameLost);
        EventSystem.RegisterListener<OpenMainMenu>(OnShowMainMenu);

        ShowMainMenu();
    }
    
    private void OnGameLost(object incomingEvent)
    {
        ShowGameLostMenu();
    }
    private void OnShowMainMenu(object incomingEvent)
    {
        ShowMainMenu();
    }
    private void OnLevelSelectionMenuCalled(object incomingEvent)
    {
        ShowLevelSelectionMenu();
    }

    public void ShowPauseMenu()
    {
        m_pauseMenu.SetActive(true);
        m_game.SetActive(true);
        EventSystem.SendEvent<PauseRequest>(new PauseRequest());

        m_mainMenu.SetActive(false);
        m_levelsMenu.SetActive(false);
        m_lostMenu.SetActive(false);
        m_winMenu.SetActive(false);
    }
    public void ShowLevelSelectionMenu()
    {
        m_levelsMenu.SetActive(true);

        m_pauseMenu.SetActive(false);
        m_game.SetActive(false);
        m_mainMenu.SetActive(false);
        m_lostMenu.SetActive(false);
        m_winMenu.SetActive(false);
    }

    public void ShowGame()
    {
        m_game.SetActive(true);

        m_levelsMenu.SetActive(false);
        m_pauseMenu.SetActive(false);
        m_mainMenu.SetActive(false);
        m_lostMenu.SetActive(false);
        m_winMenu.SetActive(false);

        EventSystem.SendEvent<RunNewLevel>(new RunNewLevel() { m_fieldSize = new Vector2Int(45, 25), m_clearCellChance = 0.7f });
    }

    public void ShowMainMenu()
    {
        m_mainMenu.SetActive(true);

        m_levelsMenu.SetActive(false);
        m_pauseMenu.SetActive(false);
        m_game.SetActive(false);
        m_lostMenu.SetActive(false);
        m_winMenu.SetActive(false);
    }
    public void ShowGameLostMenu()
    {
        m_lostMenu.SetActive(true);
        m_game.SetActive(true);

        m_mainMenu.SetActive(false);
        m_levelsMenu.SetActive(false);
        m_pauseMenu.SetActive(false);
        m_winMenu.SetActive(false);
    }
}
