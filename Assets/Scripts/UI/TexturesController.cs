﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexturesController : MonoBehaviour
{
    public enum ETexturesType
    {
        Grass,
        Ground,
        Mine,
        Column,
        Row,
    }

    public Sprite[] m_grass;
    public Sprite[] m_ground;
    public Sprite[] m_mine;
    public Sprite[] m_column;
    public Sprite[] m_row;

    public Sprite GetRandomSpriteOfType(ETexturesType type)
    {
        Sprite[] sprites = GetCorrectArray(type);
        if (sprites != null &&
            sprites.Length > 0)
        {
            return sprites[Random.Range(0, sprites.Length)];
        }
        Debug.LogError("There is no sprites of this type");
        return null;
    }

    Sprite[] GetCorrectArray(ETexturesType type)
    {
        switch(type)
        {
            case ETexturesType.Grass:
                {
                    return m_grass;
                }
            case ETexturesType.Ground:
                {
                    return m_ground;
                }
            case ETexturesType.Mine:
                {
                    return m_mine;
                }
            case ETexturesType.Column:
                {
                    return m_column;
                }
            case ETexturesType.Row:
                {
                    return m_row;
                }
            default:
                Debug.LogError("Sprites of this type are not supprted");
                return null;
        }
    }
}
