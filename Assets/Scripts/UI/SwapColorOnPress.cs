﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapColorOnPress : MonoBehaviour
{
    public Color m_color1;
    public Color m_color2;
    public Image m_buttonImage;

    private void Start()
    {
        SwapColor();
    }
    public void SwapColor()
    {
        m_buttonImage.color = m_color2;
        Color tmp = m_color2;
        m_color2 = m_color1;
        m_color1 = tmp;
    }
}
