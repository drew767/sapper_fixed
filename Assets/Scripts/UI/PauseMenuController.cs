﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuController : MonoBehaviour
{
    public void Restart()
    {
        EventSystem.SendEvent(new RestartGame());
        gameObject.SetActive(false);
    }
    public void OpenLevelsMenu()
    {
        EventSystem.SendEvent(new LevelSelectionMenu());
    }
    public void ContinueCurrentGame()
    {
        EventSystem.SendEvent(new UnpauseRequest());
        gameObject.SetActive(false);
    }

    public void ExitToMainMenu()
    {
        EventSystem.SendEvent(new OpenMainMenu());
    }
}
