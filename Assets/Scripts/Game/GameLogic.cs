﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public bool m_gameIsPlaying;
    public enum CellType
    {
        Empty,
        Mine
    }

    public int m_columns;
    public int m_rows;

    private float m_clearCellChance;

    private CellType[][] m_field;
    private List<int>[] m_rowsInfo;
    private List<int>[] m_columnsInfo;

    private int m_minesNum;
    private int m_cleanCellsNum;


    // Start is called before the first frame update
    void Start()
    {
        EventSystem.RegisterListener<RunNewLevel>(OnNewLevelRequested);
        EventSystem.RegisterListener<PauseRequest>(OnGamePaused);
        EventSystem.RegisterListener<UnpauseRequest>(OnGameUnpaused); 
        EventSystem.RegisterListener<RestartGame>(OnGameRestart); 
    }

    void InitializeField()
    {
        int minesLeftToSet = m_minesNum;
        while (minesLeftToSet != 0)
        {
            for (int i = 0; i < m_rows; i++)
            {
                for (int j = 0; j < m_columns; j++)
                {
                    int randomValue = (int)UnityEngine.Random.Range(0.0f, 100.0f);
                    if (m_field[i][j] != CellType.Mine && randomValue < 15)
                    {
                        m_field[i][j] = CellType.Mine;
                        minesLeftToSet--;
                        if (minesLeftToSet == 0)
                        {
                            return;
                        }
                    }
                }
            }
        }
    }

    void GenerateFieldInfo()
    {
        GenerateRowsInfo();
        GenerateColumnsInfo();
    }
    void GenerateColumnsInfo()
    {
        m_columnsInfo = new List<int>[m_columns];
        int counter = 0;
        for (int i = 0; i < m_columns; i++)
        {
            m_columnsInfo[i] = new List<int>();
            for (int j = 0; j < m_rows; j++)
            {
                if (m_field[j][i] == CellType.Empty)
                {
                    counter++;
                }
                else if (counter != 0)
                {
                    m_columnsInfo[i].Add(counter);
                    counter = 0;
                }
            }

            if (counter != 0)
            {
                m_columnsInfo[i].Add(counter);
                counter = 0;
            }
        }
    }
    void GenerateRowsInfo()
    {
        m_rowsInfo = new List<int>[m_rows];
        int counter = 0;
        for (int i = 0; i < m_rows; i++)
        {
            m_rowsInfo[i] = new List<int>();
            for (int j = 0; j < m_columns; j++)
            {
                if (m_field[i][j] == CellType.Empty)
                {
                    counter++;
                }
                else if (counter != 0)
                {
                    m_rowsInfo[i].Add(counter);
                    counter = 0;
                }
            }

            if (counter != 0)
            {
                m_rowsInfo[i].Add(counter);
                counter = 0;
            }
        }
    }
    public void OnCellPressed(VisualFieldCellController cell, int x, int y)
    {
        if (!m_gameIsPlaying)
        {
            return;
        }

        switch (m_field[x][y])
        {
            case CellType.Mine:
                {
                    FindObjectOfType<LiveController>().ReduceLive();
                    cell.SetFailed();
                    break;
                }
            case CellType.Empty:
                {
                    cell.SetSuccess();
                    break;
                }
            default:
                {
                    cell.SetUnhandledError();
                    break;
                }
        }
    }

    private void SetupPrepareGameToStart()
    {
        m_gameIsPlaying = true;
        CreateField();
        InitializeField();
        GenerateFieldInfo();
        EventSystem.SendEvent(new DrawGameField());
        EventSystem.SendEvent(new NewGameStarted());
    }
    void CreateField()
    {
        int fieldSize = m_columns * m_rows;
        m_cleanCellsNum = (int)(fieldSize * m_clearCellChance);
        m_minesNum = fieldSize - m_cleanCellsNum;

        m_field = new CellType[m_rows][];
        for (int i = 0; i < m_rows; i++)
        {
            m_field[i] = new CellType[m_columns];
            for (int j = 0; j < m_columns; j++)
            {
                m_field[i][j] = CellType.Empty;
            }
        }
    }

    public string GetRowInfo(int rowIndex)
    {
        string str = "";
        for (int j = 0; j < m_rowsInfo[rowIndex].Count; j++)
        {
            str += m_rowsInfo[rowIndex][j] + " ";
        }
        return str;
    }
    public string GetColumnInfo(int columnIndex)
    {
        string str = "";
        for (int j = 0; j < m_columnsInfo[columnIndex].Count; j++)
        {
            str += m_columnsInfo[columnIndex][j] + "\n";
        }
        return str;
    }

    /// <summary>
    /// EVENT CALLBACKS //////////////////////////////////////////////////////////////////
    /// </summary>
    /// <param name="incomingEvent"></param>
    private void OnNewLevelRequested(object incomingEvent)
    {
        RunNewLevel newLevelEvent = (RunNewLevel)incomingEvent;
        m_columns = newLevelEvent.m_fieldSize.x;
        m_rows = newLevelEvent.m_fieldSize.y;
        m_clearCellChance = newLevelEvent.m_clearCellChance;

        SetupPrepareGameToStart();
    }
    private void OnGamePaused(object incomingEvent)
    {
        m_gameIsPlaying = false;
    }
    private void OnGameRestart(object incomingEvent)
    {
        SetupPrepareGameToStart();
    }
    private void OnGameUnpaused(object incomingEvent)
    {
        m_gameIsPlaying = true;
    }

    /// <summary>
    /// DEBUG PRINT OUT ////////////////////////////////////////////////////////////////// 
    /// </summary>
    void PrintField()
    {
        Debug.Log("Field Size = " + m_columns * m_rows + "; Number of Mines = " + m_minesNum);

        string str = "";
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_columns; j++)
            {
                str += m_field[i][j];
            }
            str += '\n';
        }
        Debug.Log(str);
    }
    void PrintFieldInfo()
    {
        {
            string str = "Row 0: ";
            for (int i = 0; i < m_rows;)
            {
                for (int j = 0; j < m_rowsInfo[i].Count; j++)
                {
                    str += m_rowsInfo[i][j] + ", ";
                }
                Debug.Log(str);
                str = "Row " + (++i) + ": ";
            }
        }
        {
            string str = "Column 0: ";
            for (int i = 0; i < m_columns;)
            {
                for (int j = 0; j < m_columnsInfo[i].Count; j++)
                {
                    str += m_columnsInfo[i][j] + ", ";
                }
                Debug.Log(str);
                str = "Column " + (++i) + ": ";
            }
        }
    }
}
