﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiveController : MonoBehaviour
{
    private int m_lives;
    public Image[] m_img;

    public Color m_liveColor;
    public Color m_deadColor;

    private GameLogic m_gameLogic;

    void SetupLives()
    {
        m_lives = 3;
        for (int i = 0; i < m_img.Length; i++)
        {
            m_img[i].color = m_liveColor;
        }
    }
    void Start()
    {
        EventSystem.RegisterListener<NewGameStarted>(OnNewLevelRequested);
        m_gameLogic = FindObjectOfType<GameLogic>();
    }
    private void OnNewLevelRequested(object incomingEvent)
    {
        SetupLives();
    }
    public void ReduceLive()
    {
        if (m_lives > 0)
        {
            m_img[m_lives - 1].color = m_deadColor;
        }
        m_lives--;
        if (m_lives <= 0)
        {
            m_gameLogic.m_gameIsPlaying = false;
            EventSystem.SendEvent(new GameLost());
        }
    }
}
