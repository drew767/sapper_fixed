﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldSpawner : MonoBehaviour
{
    public GameObject m_cellPrefab;
    private GameLogic m_gameLogic;
    private float m_cellWidth;

    void Start()
    {
        m_gameLogic = FindObjectOfType<GameLogic>();
        m_cellWidth = m_cellPrefab.transform.GetComponent<RectTransform>().rect.size.y;
        EventSystem.RegisterListener<DrawGameField>(OnNewLevelGenerationRequest);
    }
    private void OnNewLevelGenerationRequest(object incomingEvent)
    {
        ClearAll();
        SpawnField();
    }
    private void ClearAll()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    private void SpawnField()
    {
        for (int i = 0; i < m_gameLogic.m_rows; i++)
        {
            for (int j = 0; j < m_gameLogic.m_columns; j++)
            {
                GameObject cell = Instantiate(m_cellPrefab) as GameObject;
                var cellController = cell.GetComponent<VisualFieldCellController>();
                cellController.m_x = i;
                cellController.m_y = j;

                Vector3 position = Vector3.zero;
                position.x += (m_cellWidth * j);
                position.y -= (m_cellWidth * i);

                cell.transform.SetParent(transform, false);
                cell.transform.localScale = Vector3.one;
                cell.transform.localPosition = position;
            }
        }
    }
}
