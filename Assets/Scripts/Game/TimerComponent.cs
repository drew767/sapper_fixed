﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerComponent : MonoBehaviour
{
    private DateTime m_previousFrame;
    private TimeSpan m_duration = TimeSpan.Zero;
    private bool m_tick = false;
    public TextMeshProUGUI m_timerLabel;

    void Start()
    {
        EventSystem.RegisterListener<PauseRequest>(OnPauseRequested);
        EventSystem.RegisterListener<UnpauseRequest>(OnContinueGame); 
        EventSystem.RegisterListener<GameLost>(OnContinueGame);
        EventSystem.RegisterListener<NewGameStarted>(OnNewGameStarted);
    }

    private void OnPauseRequested(object incomingEvent)
    {
        m_tick = false;
    }
    private void OnContinueGame(object incomingEvent)
    {
        m_previousFrame = DateTime.Now;
        m_tick = true;
    }
    private void OnGameLost(object incomingEvent)
    {
        StopTimer();
    }
    private void OnNewGameStarted(object incomingEvent)
    {
        StartTimer();
    }

    void Update()
    {
        if (m_tick)
        {
            m_duration += DateTime.Now - m_previousFrame;
            SetTimeToText();
        }
        m_previousFrame = DateTime.Now;
    }

    void SetTimeToText()
    {
        String str = "";
        if (m_duration.Minutes < 10)
        {
            str += "0";
        }
        str += m_duration.Minutes + " : ";

        if (m_duration.Seconds < 10)
        {
            str += "0";
        }
        str += m_duration.Seconds;
        m_timerLabel.text = str;
    }

    private void StartTimer()
    {
        m_previousFrame = DateTime.Now;
        m_duration = TimeSpan.Zero;
        m_tick = true;
    }

    private void StopTimer()
    {
        m_tick = false;
    }
}
