﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualFieldCellController : MonoBehaviour
{
    Image m_Image;
    public int m_x;
    public int m_y;
    private TexturesController m_texturesController;
    public Sprite m_SuccessSprite; //Drag your success sprite here in inspector.
    public Sprite m_FailSprite; //Drag your fail sprite here in inspector.
    public Sprite m_ErrorSprite; //Drag your fail sprite here in inspector.

    // Start is called before the first frame update
    void Start()
    {
        m_Image = gameObject.GetComponent<Image>();
        m_texturesController = FindObjectOfType<TexturesController>();
        Reset();
    }

    public void OnButtonPressed()
    {
        var gameLogic = FindObjectOfType<GameLogic>();
        gameLogic.OnCellPressed(this, m_x, m_y);
    }

    public void SetFailed()
    {
        m_Image.sprite = m_FailSprite;
    }

    public void Reset()
    {
        m_Image.sprite = m_texturesController.GetRandomSpriteOfType(TexturesController.ETexturesType.Grass);
    }
    public void SetSuccess()
    {
        m_Image.sprite = m_SuccessSprite;
    }
    public void SetUnhandledError()
    {
        m_Image.sprite = m_ErrorSprite;
    }
}
