﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RowStatsListSpawner : MonoBehaviour
{
    public GameObject m_stats;
    private GameLogic m_gameLogic;
    private float m_cellSize;
    private TexturesController m_texturesController;

    void Start()
    {
        m_texturesController = FindObjectOfType<TexturesController>();
        m_gameLogic = FindObjectOfType<GameLogic>();
        m_cellSize = m_stats.transform.GetChild(0).GetComponent<RectTransform>().rect.size.y;
        EventSystem.RegisterListener<DrawGameField>(OnNewLevelGenerationRequest);
    }
    private void OnNewLevelGenerationRequest(object incomingEvent)
    {
        ClearAll();
        SpawnList();
    }
    private void ClearAll()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    public void SpawnList()
    {
        for (int i = 0; i < m_gameLogic.m_rows; i++)
        {
            GameObject stats = Instantiate(m_stats) as GameObject;
            stats.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = m_gameLogic.GetRowInfo(i);

            Vector3 position = Vector3.zero;
            position.y -= (m_cellSize * i);

            stats.transform.SetParent(transform, false);
            stats.transform.localScale = Vector3.one;
            stats.transform.localPosition = position;

            stats.GetComponent<Image>().sprite = m_texturesController.GetRandomSpriteOfType(TexturesController.ETexturesType.Row);
        }
    }
}