﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepositionToCenter : MonoBehaviour
{
    Vector2 m_previousScreenSize = new Vector2();
    private GameLogic m_gameLogic;
    private bool m_tryReupdate;

    void Start()
    {
        m_tryReupdate = true;
        m_gameLogic = FindObjectOfType<GameLogic>();
    }
    void UpdateGameAreaPosition()
    {
        Transform gameInfo = transform.Find("Stats").transform;
        Transform column = transform.Find("RowStatsList").transform;

        if (gameInfo.childCount == 0 ||
            column.childCount == 0)
        {
            m_tryReupdate = true;
            return;
        }

        var gameInfoSize = gameInfo.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;
        var columnSize = column.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;

        Vector3 position = Vector3.zero;
        Vector2 size = transform.GetComponent<RectTransform>().rect.size;

        position.y = (size.y - ((columnSize * m_gameLogic.m_rows) + gameInfoSize)) / 2;
        position.x = -(size.x - ((columnSize * m_gameLogic.m_columns) + gameInfoSize)) / 2;

        transform.parent.localPosition = -position;
        m_tryReupdate = false;
    }

    private void Update()
    {
        Vector2 currentScreenSize = new Vector2();
        currentScreenSize.x = Screen.width;
        currentScreenSize.y = Screen.height;

        if (currentScreenSize != m_previousScreenSize || m_tryReupdate)
        {
            m_previousScreenSize = currentScreenSize;
            UpdateGameAreaPosition();
        }
    }
}
