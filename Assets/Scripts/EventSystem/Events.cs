﻿using UnityEngine;

// Event Example
//class PauseMenuActivated
//{
//}
//EventSystem.RegisterListener<PauseMenuActivated>(OnPauseRequested);
// Event calls
//private void OnPauseRequested(object incomingEvent) {}
//EventSystem.SendEvent(new PauseMenuActivated() { });

class RunNewLevel
{
    public Vector2Int m_fieldSize;
    public float m_clearCellChance;
}
class PauseRequest
{
}
class UnpauseRequest
{
}
class DrawGameField
{
}
class GameLost
{
}
class LevelSelectionMenu
{
}
class NewGameStarted
{
}
class RestartGame
{
}
class OpenMainMenu
{
}